import React, { useState } from "react";
import { Container, Form, Button } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

export default function FormComponent() {
  const [inputFields, setInputFields] = useState([
    {
      wantedJob: "",
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      country: "",
      city: "",
      address: "",
      nationality: "",
      dob: "",
      pod: "",
    },
  ]);

  const handleInputChange = (index, event) => {
    let data = [...inputFields];
    data[index][event.target.name] = event.target.value;
    setInputFields(data);
  };

  const addFields = () => {
    let newField = {
      wantedJob: "",
      firstName: "",
      lastName: "",
      email: "",
      phone: "",
      country: "",
      city: "",
      address: "",
      nationality: "",
      dob: "",
      pod: "",
    };
    setInputFields([...inputFields, newField]);
  };

  const submit = (e) => {
    e.preventDefault();
    console.log(inputFields);
  };

  const removeFields = (index) => {
    let data = [...inputFields];
    data.splice(index, 1);
    setInputFields(data);
  };

  return (
    <Container className="mt-5">
      <Form onSubmit={submit}>
        {inputFields.map((input, index) => {
          return (
            <div key={index} className="mb-3">
              <input
                name="wantedJob"
                placeholder="enter your dreamed job"
                value={input.wantedJob}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <input
                name="firstName"
                placeholder="enter your first name"
                value={input.firstName}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <input
                name="lastName"
                placeholder="enter your last name"
                value={input.lastName}
                onChange={(event) => handleInputChange(index, event)}
              ></input><br></br>
              <input
                name="email"
                placeholder="enter your email"
                value={input.email}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <input
                name="phone"
                placeholder="enter your phone number"
                value={input.phone}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <input
                name="country"
                placeholder="enter your country"
                value={input.country}
                onChange={(event) => handleInputChange(index, event)}
              ></input><br></br>
              <input
                name="city"
                placeholder="enter your city"
                value={input.city}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <input
                name="address"
                placeholder="enter your address"
                value={input.address}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <input
                name="nationality"
                placeholder="enter your nationality"
                value={input.nationality}
                onChange={(event) => handleInputChange(index, event)}
              ></input><br></br>
              <input
                name="dob"
                placeholder="enter your date of birth"
                value={input.dob}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <input
                name="pod"
                placeholder="enter your place of birth"
                value={input.pod}
                onChange={(event) => handleInputChange(index, event)}
              ></input>
              <Button onClick={() => removeFields(index)} variant="danger">
                Remove Field
              </Button>
            </div>
          );
        })}
      </Form>
      <Button onClick={addFields} variant="success">
        Add new field
      </Button>
      <Button onClick={submit}>Submit</Button>
    </Container>
  );
}
